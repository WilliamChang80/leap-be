<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['course_name'];

    public function classCurriculum(){
        return $this->hasMany('App\Curriculum');
    }

    public function userClass(){
        return $this->hasMany('App\UserClass');
    }

    public function assignment(){
        return $this->hasOne('App\Assignment');
    }
}
