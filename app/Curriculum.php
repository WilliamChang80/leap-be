<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $fillable = ['course_id','term_id','meeting','material'];

    public function course(){
        return $this->belongsTo('App\Course');
    }
    
    public function classSchedule(){
        return $this->hasMany('App\ClassSchedule');
    }

    public function term(){
        return $this->belongsTo('App\Term');
    }
}
