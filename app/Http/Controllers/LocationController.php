<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Location;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $location = Location::All();
        $status = 200;
        $message = "ok";
        if($location==null){
            $status = 502;
            $message = "Data Not Found";
        } 
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $location
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new Location;
        $location->location_name = $request->location;
        $location->created_at = Carbon::now();
        $location->save();
        
        $status = 201;
        $message = "Successfully Created Location";

        if(!$location){
            $status = 400;
            $message = "Failed Created Location";    
        }
        return response()->json([
            "status" => $status,
            "message" => $message,
            'data' => $location
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        $status = 200;
        $message = "ok";
        if(!$location){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $location
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        
        $location->location_name = $request->location;
        $location->save();
        $message = "Succefully Update Data";
        $status = 200;
        
        if(!$location){
            $message = "Failed Update Data";
            $status = 400;
        }

        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $location
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $status = 200;
        $message = "Deleted Data";
        if(!$location){
            $status = 400;
            $message = "No Data Found";
        }
        $location->delete();

        return response()->json([
            "status" => $status,
            "message" => $message
        ]);
    }
}
