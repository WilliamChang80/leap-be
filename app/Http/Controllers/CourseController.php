<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use Validator;

class CourseController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make(request()->all(), [
            'course_name'  => 'required',
        ]);
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 400);
        }
        $course = new Course;
        $course->course_name = $request->course_name;
        $course->save();
        $course->assignment()->create();

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function index(){
        $course = Course::all();
        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'data' => $course
        ]);
    }

    public function update(Course $course, Request $request) {
        $validator = Validator::make(request()->all(), [
            'course_name'  => 'required',
        ]);
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 400);
        }
        $course->course_name = $request->course_name;
        $course->save();
        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function delete(Course $course){
        $course->delete();
        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function show(Course $course){
        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'data' => $course
        ]);
    }
}
