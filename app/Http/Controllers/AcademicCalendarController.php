<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AcademicCalendar;
use Validator;

class AcademicCalendarController extends Controller
{
    public function createAcademicCalendar(Request $request){
        $validator = Validator::make(request()->all(),[
            'period' => 'required|numeric',
            'term_id' => 'required|numeric',
            'begin_date'=>'required|date',
            'end_date' =>'required|date|after_or_equal:begin_date',
            'description' => 'required|string',
            'calendar_status_id'=>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }

        $academicCalendar = new AcademicCalendar;
        $academicCalendar->period = $request->period;
        $academicCalendar->term_id = $request->term_id;
        $academicCalendar->begin_date = $request->begin_date;
        $academicCalendar->end_date = $request->end_date;
        $academicCalendar->description = $request->description;
        $academicCalendar->calendar_status_id = $request->calendar_status_id;
        $academicCalendar->save();

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function updateAcademicCalendar(Request $request, AcademicCalendar $academicCalendar){
        $validator = Validator::make(request()->all(),[
            'period' => 'required|numeric',
            'term_id' => 'required|numeric',
            'begin_date'=>'required|date',
            'end_date' =>'required|date|after_or_equal:begin_date',
            'description' => 'required|string',
            'calendar_status_id'=>'required|numeric'
        ]);

        $academicCalendar->update($request->only('period','term_id','begin_date','end_date','description','calendar_status_id'));

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function getAcademicCalendar(AcademicCalendar $academicCalendar){
        $id = $academicCalendar->id;
        $academicCalendar = AcademicCalendar::with('term','calendarStatus')->where('id',$id)->first();
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $academicCalendar
        ]);
    }

    public function getAcademicCalendarList(){
        $academicCalendar = AcademicCalendar::with('term','calendarStatus')->get();
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $academicCalendar
        ]);
    }

    public function removeAcademicCalendar(AcademicCalendar $academicCalendar){
        $academicCalendar->delete();
        return response()->json([
            'status' => 200,
            'message' => "Data deleted successfuly"
        ]);
    }
}
