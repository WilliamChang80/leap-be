<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = ['mid_score','final_score','class_detail_id'];

    protected $hidden=['laravel_through_key'];

    public function classDetail(){
        return $this->belongsTo('App\ClassDetail','class_detail_id');
    }
}
