<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/lecturers'], function () {
    Route::get('/', 'UserController@getAllLecturer')->name('Lecturer.index');
    Route::post('', 'UserController@createLecturer')->name('Lecturer.store');
    Route::patch('/{lecturer}', 'UserController@updateLecturer')->name('Lecturer.update');
    Route::delete('/{lecturer}', 'UserController@deleteLecturer')->name('Lecturer.destroy');
});

Route::group(['prefix' => '/members'], function () {
    Route::get('/', 'UserController@getAllMember')->name('Member.index');
    Route::post('', 'UserController@createMember')->name('Member.store');
    Route::get('/{member}', 'UserController@getMember')->name('Member.show');
    Route::patch('/{member}', 'UserController@updateMember')->name('Member.update');
    Route::delete('/{member}', 'UserController@deleteMember')->name('Member.destroy');
});


Route::group(['prefix'=>'/locations','middleware'=>'cors'],function(){
    Route::get('/', 'LocationController@index')->name('Location.index');
    Route::post('/', 'LocationController@store')->name('Location.store');
    Route::get('/{location}', 'LocationController@show')->name('Location.show');
    Route::patch('/{location}', 'LocationController@update')->name('Location.update');
    Route::delete('/{location}', 'LocationController@destroy')->name('Location.destroy');
});


Route::group(['prefix' => '/majors'], function () {
    Route::get('/', 'MajorController@index')->name('Major.index');
    Route::post('/', 'MajorController@store')->name('Major.store');
    Route::get('/{major}', 'MajorController@show')->name('Major.show');
    Route::patch('/{major}', 'MajorController@update')->name('Major.update');
    Route::delete('/{major}', 'MajorController@destroy')->name('Major.destroy');
});

Route::post('login', 'API\UserController@login');
// Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'cors'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('details', 'API\UserController@details');
        Route::post('change-email', 'API\UserController@changeEmail');
        Route::post('change-password', 'API\UserController@changePassword');
        Route::get('logout', 'API\UserController@logout');
        Route::group(['prefix' => '/class-schedule'], function() {
            Route::get('/next-class/{user_id}', 'ClassScheduleController@getNextClasses');
            Route::get('/previous-class', 'ClassScheduleController@getPreviousClassesWithCurriculumAndClass');
            Route::get('/next-class', 'ClassScheduleController@getNextClassesWithCurriculumAndClass');
            Route::get('/all-class-material', 'ClassScheduleController@getAllCurrentClassesMaterial');
            Route::get('/class-count', 'ClassScheduleController@getUserClassCount');
        });
        Route::group(['prefix'=>'/user-classes'],function(){
            Route::get('/{class}', 'UserClassController@getClassById');
        });

        Route::group(['prefix'=>'/absences'],function(){
            Route::get('/','AbsenceController@getUserAbsence');
            Route::get('/attendance-count', 'AbsenceController@getUserAbsenceCount');
            Route::post('/check', 'AbsenceController@getAttendanceStatus');
        });

        Route::group(['prefix' => '/lecturers'], function () {
            Route::get('/{lecturer}', 'UserController@getLecturer')->name('Lecturer.show');
        });

        Route::group(['prefix'=>'/assignments'], function() {
            Route::get('/member/assignment', 'AssignmentController@getCurrentCourseAssignment');
        });

        Route::group(['middleware' => 'check.role:admin'], function () {

        });

        Route::group(['middleware' => 'check.role:lecturer'], function () {
            Route::group(['prefix' => '/class-schedule'], function() {
                Route::get('/lecturer/next-class', 'ClassScheduleController@getLecturerNextClassesWithCurriculumAndClass');
            });
            Route::group(['middleware' => 'auth:api'],function(){
                Route::group(['prefix' => '/absences'],function(){
                    Route::get('/member', 'AbsenceController@getMemberData');
                    Route::post('/insert', 'AbsenceController@storeUserAbsence');
                });
            });
        });

        Route::group(['middleware' => 'check.role:member'], function () {
            Route::group(['prefix' => '/submit-assignment'], function () {
                Route::post('/mid', 'SubmitAssignmentController@storeMidExam');
                Route::post('/final', 'SubmitAssignmentController@storeFinalExam');
            });

        });
    });
});

Route::group(['prefix'=>'/courses'],function(){
    Route::post('/', 'CourseController@store');
    Route::get('/', 'CourseController@index');
    Route::patch('/{course}', 'CourseController@update');
    Route::delete('/{course}', 'CourseController@delete');
    Route::get('/{course}', 'CourseController@show');
});

Route::get('/config', 'ConfigController@index');
Route::patch('/config', 'ConfigController@updateConfig');

Route::group(['prefix' => '/academic-calendars'], function () {
    Route::post('/', 'AcademicCalendarController@createacademicCalendar');
    Route::patch('/{academicCalendar}', 'AcademicCalendarController@updateAcademicCalendar');
    Route::get('/{academicCalendar}', 'AcademicCalendarController@getAcademicCalendar');
    Route::delete('/{academicCalendar}', 'AcademicCalendarController@removeAcademicCalendar');
    Route::get('/', 'AcademicCalendarController@getAcademicCalendarList');
});

Route::group(['prefix'=>'/curriculum'],function(){
    Route::get('/','API\CurriculaController@getAllCurricula');
    //
    Route::get('/{curriculum}','API\CurriculaController@getCurricula');
    Route::get('/courses/{course_id}', 'API\CurriculaController@getCurriculabyCourse');
    Route::post('/','API\CurriculaController@storeCurricula');
    Route::post('/update','API\CurriculaController@updateCurricula');
    Route::delete('/{curriculum}','API\CurriculaController@deleteCurricula');
});

Route::group(['prefix' => '/scores'], function () {
    Route::get('/', 'ScoreController@getAllScore');
    Route::post('/', 'ScoreController@createScore');
    Route::get('/{score}', 'ScoreController@getScore');
    Route::patch('/{score}', 'ScoreController@updateScore');
    Route::delete('/{score}', 'ScoreController@deleteScore');
});

Route::group(['prefix'=>'/user-classes','middleware'=>'auth:api'],function(){
    Route::group(['middleware' => 'check.role:member'], function () {
        Route::get('/members', 'UserClassController@getAllMemberClass');
    });
    Route::group(['middleware' => 'check.role:lecturer'], function () {
        Route::get('/lecturers', 'UserClassController@getAllLecturerClass');
    });
    Route::get('/{class}/submit-assignments','UserClassController@getSubmittedAssignment');
    Route::get('/{classId}/scores','UserClassController@getScoreByClass');
    Route::group(['middleware' => 'check.role:admin'], function () {
        Route::get('/', 'UserClassController@getAllClass');
        Route::get('/courses/{id}','UserClassController@getClassByCourse')->name('class.admin.pages'); 
        Route::post('/', 'UserClassController@store');
        Route::patch('/{class}', 'UserClassController@update');
        Route::delete('/{class}', 'UserClassController@destroy');
    });
});

Route::group(['prefix' => '/submit-assignment'], function () {
    Route::get('/', 'SubmitAssignmentController@getAllSubmittedAssignment')->name('submittedAssignment.allSubmit');
    Route::get('/{assignment}', 'SubmitAssignmentController@show')->name('submittedAssignment.show');
    Route::post('class-details/{classDetailId}', 'SubmitAssignmentController@update')->name('submittedAssignment.update');
});

Route::group(['prefix'=>'/shifts'],function(){
    Route::get('/','ShiftController@getAllShift')->name('shift.getAllShift');
    Route::post('/','ShiftController@createShift')->name('shift.createShift');
    Route::get('/{shift}','ShiftController@getShift')->name('shift.getShift');
    Route::delete('/{shift}','ShiftController@deleteShift')->name('shift.deleteShift');
    Route::patch('/{shift}','ShiftController@editShift')->name('shift.editShift');
});

Route::group(['prefix' => '/class-schedule'], function() {
    Route::get('/{user_class_id}', 'ClassScheduleController@getAllClassSchedule'); 
    Route::post('/', 'ClassScheduleController@createClassSchedule');
    Route::patch('/', 'ClassScheduleController@updateAllClassSchedules');
    Route::patch('/{id}', 'ClassScheduleController@updateClassScheduleTime');
});

Route::group(['prefix'=>'/class-details','middleware'=>'auth:api'],function(){
    Route::post('/','ClassDetailController@createClassDetail');
    Route::patch('/{classDetail}','ClassDetailController@updateClassDetail');
    Route::get('/attendace','ClassDetailController@countMemberAttandences');
    Route::delete('/{classDetail}','ClassDetailController@deleteClassDetail');
});
Route::group(['prefix'=>'/assignments'],function(){
    Route::get('/','AssignmentController@getAllAssignment');
    Route::post('/','AssignmentController@storeAssignment');
    Route::post('/courses/{id}','AssignmentController@updateAssignment');
});

Route::group(['prefix'=>'/absences'],function(){
    Route::patch('/','AbsenceController@update')->name('Absence.update');
    Route::get('/member-data', 'AbsenceController@getMemberData');
});