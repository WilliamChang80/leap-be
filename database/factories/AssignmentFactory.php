<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Assignment;
use Faker\Generator as Faker;

$factory->define(Assignment::class, function (Faker $faker) {
    return [
        'mid_exam_name' => $faker->name(),
        'mid_exam_file' => $faker->url(),
        'final_exam_name' => $faker->name(),
        'final_exam_file' => $faker->url(),
        'final_judgment_criteria' => $faker->sentence(5),
        'mid_judgment_criteria' => $faker->sentence(5),
        'final_requirement' => $faker->sentence(5),
        'mid_requirement' => $faker->sentence(5),
        'course_id' =>$faker->numberBetween($min = 1, $max = 5)
    ];
});
