<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Role::class, function ($roleName) {
    return [
        'role_name' => $roleName->name,
    ];
});
