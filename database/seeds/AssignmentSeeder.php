<?php

use Illuminate\Database\Seeder;
use App\Assignment;

class AssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(Assignment::class,10)->create();
        $faker = Faker\Factory::create();
        DB::table('assignments')->insert([
            'mid_exam_name' => "Mid Project Web Design",
            'mid_exam_file' => "1_asd_MID_1575960787.pdf",
            'final_exam_name' => "Final Project Web Design",
            'final_exam_file' => "1_asd_FINAL_1575960787.pdf",
            'final_judgment_criteria' => $faker->sentence(5),
            'mid_judgment_criteria' => $faker->sentence(5),
            'final_requirement' => $faker->sentence(5),
            'mid_requirement' => $faker->sentence(5),
            'course_id' => 1
        ]);

        DB::table('assignments')->insert([
            'mid_exam_name' => "Mid Project Web Programming",
            'mid_exam_file' => "2_asd_MID_1575960906.pdf",
            'final_exam_name' => "Final Project Web Programming",
            'final_exam_file' => "2_asd_FINAL_1575960906.pdf",
            'final_judgment_criteria' => $faker->sentence(5),
            'mid_judgment_criteria' => $faker->sentence(5),
            'final_requirement' => $faker->sentence(5),
            'mid_requirement' => $faker->sentence(5),
            'course_id' => 2
        ]);
    }
}
