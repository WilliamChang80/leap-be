<?php

use Illuminate\Database\Seeder;
use App\ClassSchedule;
use Carbon\Carbon;

class ClassScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::createFromDate(2019, 10, 14);
        for($i = 0; $i < 10; $i++){
            DB::table('class_schedules')->insert([
                'user_class_id'=> 1,
                'shift_id' => 1,
                'meeting_date' => $date->addDays(7),
                'curriculum_id' => $i+1,
                'day' => "Monday"
            ]);
        }
        
        $date = Carbon::createFromDate(2019, 10, 16);
        for($i = 0; $i < 10; $i++){
            DB::table('class_schedules')->insert([
                'user_class_id'=> 3,
                'shift_id' => 3,
                'meeting_date' => $date->addDays(7),
                'curriculum_id' => $i+11,
                'day' => "Wednesday"
            ]);
        }
    }
}
