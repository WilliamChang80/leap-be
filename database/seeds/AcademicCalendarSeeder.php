<?php

use App\AcademicCalendar;
use Illuminate\Database\Seeder;

class AcademicCalendarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academic_calendars')->insert([
            'period'=>"2019",
            'term_id'=>"1",
            'begin_date'=>"2019-09-15",
            'end_date'=>"2019-09-30",
            'description'=>"Register 2019-2020",
            'calendar_status_id'=>"3",
        ]);
        DB::table('academic_calendars')->insert([
            'period'=>"2019",
            'term_id'=>"1",
            'begin_date'=>"2019-10-01",
            'end_date'=>"2019-12-21",
            'description'=>"Routine Odd",
            'calendar_status_id'=>"2",
        ]);
        DB::table('academic_calendars')->insert([
            'period'=>"2019",
            'term_id'=>"1",
            'begin_date'=>"2019-12-22",
            'end_date'=>"2019-12-31",
            'description'=>"Holiday",
            'calendar_status_id'=>"1",
        ]);
        DB::table('academic_calendars')->insert([
            'period'=>"2019",
            'term_id'=>"2",
            'begin_date'=>"2020-01-01",
            'end_date'=>"2020-01-04",
            'description'=>"Holiday",
            'calendar_status_id'=>"1",
        ]);
        DB::table('academic_calendars')->insert([
            'period'=>"2019",
            'term_id'=>"2",
            'begin_date'=>"2020-01-05",
            'end_date'=>"2020-05-31",
            'description'=>"Even Routine",
            'calendar_status_id'=>"2",
        ]);
        DB::table('academic_calendars')->insert([
            'period'=>"2019",
            'term_id'=>"2",
            'begin_date'=>"2020-06-01",
            'end_date'=>"2020-09-12",
            'description'=>"Holiday",
            'calendar_status_id'=>"1",
        ]);
    }
}
