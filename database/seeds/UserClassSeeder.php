<?php

use Illuminate\Database\Seeder;
use App\UserClass;

class UserClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_classes')->insert([
            'period' => 2019,
            'term_id' => 1,
            'location_id' => 2,
            'course_id' => 1,
            'lecturer_id' => 2,
            'master_day' => "Monday",
            'master_shift_id' => 1,
            'capacity' => 8
        ]);

        DB::table('user_classes')->insert([
            'period' => 2019,
            'term_id' => 2,
            'location_id' => 2,
            'course_id' => 1,
            'lecturer_id' => 2,
            'master_day' => "Tuesday",
            'master_shift_id' => 2,
            'capacity' => 8
        ]);

        DB::table('user_classes')->insert([
            'period' => 2019,
            'term_id' => 1,
            'location_id' => 2,
            'course_id' => 2,
            'lecturer_id' => 3,
            'master_day' => "Wednesday",
            'master_shift_id' => 3,
            'capacity' => 8
        ]);

        DB::table('user_classes')->insert([
            'period' => 2019,
            'term_id' => 2,
            'location_id' => 2,
            'course_id' => 2,
            'lecturer_id' => 3,
            'master_day' => "Thursday",
            'master_shift_id' => 4,
            'capacity' => 8
        ]);
    }
}
