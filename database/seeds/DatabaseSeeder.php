<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MajorSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(TermSeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(ShiftSeeder::class);
        $this->call(UserClassSeeder::class);
        $this->call(ClassDetailSeeder::class);
        // $this->call(ScoreSeeder::class);
        $this->call(CalendarStatusSeeder::class);
        $this->call(AssignmentSeeder::class);
        $this->call(CurriculumSeeder::class);
        $this->call(AcademicCalendarSeeder::class);
        $this->call(ClassScheduleSeeder::class);
        $this->call(SubmittedAssignmentSeeder::class);
        $this->call(AbsenceSeeder::class);
    }
}
