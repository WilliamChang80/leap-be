<?php

use Illuminate\Database\Seeder;
use App\Location;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            'location_name'=>"Alam Sutera"
        ]);

        DB::table('locations')->insert([
            'location_name'=>"Kemanggisan"
        ]);
    }
}
