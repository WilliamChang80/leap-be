<?php

use Illuminate\Database\Seeder;
use App\ClassDetail;

class ClassDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('class_details')->insert([
            'user_class_id'=> 1,
            'member_id'=> 4 
        ]);

        // DB::table('class_details')->insert([
        //     'user_class_id'=> 2,
        //     'member_id'=> 4 
        // ]);

        DB::table('class_details')->insert([
            'user_class_id'=> 3,
            'member_id'=> 5
        ]);
    }
}
