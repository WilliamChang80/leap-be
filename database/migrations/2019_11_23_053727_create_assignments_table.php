<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mid_exam_name')->nullable();
            $table->string('mid_exam_file')->nullable();
            $table->string('mid_requirement')->nullable();
            $table->string('mid_judgment_criteria')->nullable();
            $table->string('final_exam_name')->nullable();
            $table->string('final_exam_file')->nullable();
            $table->string('final_requirement')->nullable();
            $table->string('final_judgment_criteria')->nullable();
            $table->bigInteger('course_id')->unsigned();
            $table->timestamps();

            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
